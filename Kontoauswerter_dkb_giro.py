
'''This is the main file getting all actions accomplished. Little interaction with user is needed and given here.'''


import numpy as np
import pandas as pd
import datetime
import locale
from Base_Functions import data_processer
from Base_Functions import plotters

locale.setlocale(locale.LC_ALL, 'de_DE.utf8')
mydateparser = lambda x: pd.datetime.strptime(x, "%d.%m.%Y")


folder_rawdata=input('Bitte geben Sie den genauen Dateipfad des Ordners an, in dem sich die Rohdaten befinden!')

writer_excel = pd.ExcelWriter('./Ergebnisse/Ergebnisse DKB Giro/Auswertungstabelle.xlsx', engine='xlsxwriter')

##Import and process comdirect data

filename=folder_rawdata+input('Bitte geben Sie den genauen Dateinamen der Girokontodaten inklusive Dateiendung ein!')

raw_data=pd.read_csv(filename,sep=";",encoding="iso8859_15",skiprows=6,index_col=11,thousands='.',decimal=',',engine='python',parse_dates=[0,1],date_parser=mydateparser).reset_index(drop=True)
raw_data.drop(raw_data.columns[[8,9,10]],axis=1,inplace=True)

print('Die Rohdaten werden jetzt in eine Excel-Datei ausgegeben')
raw_data.to_excel(writer_excel,sheet_name='Rohdaten',index=False,header=['Buchungstag', 'Wertstellung', 'Buchungstext','Auftraggeber / Begünstigter', 'Verwendungszweck', 'Kontonummer', 'BLZ', 'Betrag (EUR)'])

## process data to use it
dicttype='giro'
basis_data=data_processer.processdkbgiro(dicttype,raw_data)
basis_data.loc[basis_data['cat']=='Gemeinschafts-\nkonto','cat']='Einzahlungen'
basis_data.loc[basis_data['cat']=='DKB-Bargeld','cat']='Überweisung\nDKB Giro Dani'




print('Die kategorisierten Daten werden jetzt in eine Excel-Datei ausgegeben')

basis_data.to_excel(writer_excel,sheet_name='Aufbereitete Daten',index=False,header=["Buchungstag","Wertstellung (Valuta)","Vorgang","Buchungstext","Beträge in EUR","Monat","Kategorie"])


##Monthly data
data_month=basis_data.groupby('month',sort=False)['val'].sum().reset_index() ##get monthly overview
data_month=data_month.sort_values(['val'],ascending=False) ##sort monthly data


##Get holiday to plot separately
data_hol=basis_data.loc[basis_data['cat'].str.contains('Urlaub')]
data_hol.to_excel(writer_excel,sheet_name='Urlaubsausgaben',index=False,header=["Buchungstag","Wertstellung (Valuta)","Vorgang","Buchungstext","Beträge in EUR","Monat","Kategorie"])


## Get Data based on categories
data_cat,summe_rest=data_processer.makecatdata(basis_data,data_month)
data_cat.to_excel(writer_excel,sheet_name='Übersicht nach Kategorie.xlsx',index=False,header=["Kategorie","Beträge in EUR","Beträge pro Monat"])


writer_excel.save() ##Save Excel file

'''Plotting part'''
printfolder='Ergebnisse/Ergebnisse DKB Giro/'
print('Die Plots werden jetzt erstellt und gespeichert!')

##Plot month data
monthtitle='Monatsaufstellung Umsätze Gemeinschaftskonto'
plotters.monthplotter(data_month,monthtitle,printfolder)

##plot the total cost overview 
boxplottitle='Boxplot sämtlicher Umsätze nach Kategorie'
plotters.boxplotter(basis_data,data_month,boxplottitle,printfolder) #Boxplot
violintitle='Detaillierte Übersicht Verteilung Umsätze nach Kategorie'
plotters.violinplotter(basis_data,data_month,violintitle,printfolder) #Violinplot


##Sort for Top3 and Income
top3=data_cat.iloc[:4].reset_index(drop=True)

##Adjusted TOP3 without invest
top3_adj=data_cat[data_cat['cat'].isin(['Einzahlungen','Lohn','Miete','Gesamtsumme'])]
top3_adj=top3_adj.append(pd.DataFrame([['Kosten ohne\nInvest und Miete', summe_rest,summe_rest/data_month['month'].nunique()]],columns=list(top3.columns)),ignore_index=True)
top3_adj=top3_adj.sort_values(['val'],ascending=False).reset_index(drop=True)

##plot overview & top3 plots
plotinfo_overview=('Überblick Kosten und Einzahlungen','Einzahlungen und TOP3-Kostenblöcke','Einzahlungen, Miete & Restliche Kosten','TOP3 & Rest-Kosten.jpg')
plotters.overviewplot(top3,top3_adj,data_month,plotinfo_overview,printfolder)

##Get single costs with and without holiday
cost_total=data_cat[data_cat['cat'].isin(['Einzahlungen','Lohn','Gesamtsumme'])==False].reset_index(drop=True)
cost_hol=cost_total.loc[cost_total['cat'].str.contains('Urlaub')]
cost_nothol=cost_total.loc[cost_total['cat'].str.contains('Urlaub')==False]


##Put all relevant costs together
costs=[cost_total,cost_hol,cost_nothol]

plotinfo_costs=[('Detaillierte Übersicht Kostenkategorien inklusive Urlaube','Komplettübersicht.jpg'),('Detaillierte Übersicht Urlaube','Urlaubsübersicht.jpg'),('Detaillierte Übersicht Kostenkategorien ohne Urlaube','Übersicht ohne Urlaub.jpg')]
for i in range(0,len(costs)):
	plotters.costplotter(costs[i],data_month,plotinfo_costs[i],printfolder)


## Prepare Pie charts. Group together labels with less than 2% share

##Adjust total cost with holidays grouped together
cost_total_adj=cost_nothol.append(pd.DataFrame([['Urlaube gesamt',sum(cost_hol['val']),sum(cost_hol['val_month'])]], columns=list(cost_nothol.columns)),ignore_index=True)
cost_total_adj=cost_total_adj.sort_values('val',ascending=False).reset_index(drop=True)

##First pie costs without holidays
cost_intermediate=cost_nothol.assign(ppt=(cost_nothol['val']*100/cost_nothol['val'].sum()).round(2))
cost_intermediate.drop('val_month',axis=1,inplace=True)
cost_pie_nothol=cost_intermediate.loc[cost_intermediate['ppt']>=2.0]
cost_pie_nothol=cost_pie_nothol.append(pd.DataFrame([['Restliche mit <2%',sum(cost_intermediate.loc[cost_intermediate['ppt']<2.0]['val']),sum(cost_intermediate.loc[cost_intermediate['ppt']<2.0]['ppt'])]],columns=list(cost_intermediate.columns)),ignore_index=True)

##Second pie costs with holidays together
cost_intermediate2=cost_total_adj.assign(ppt=(cost_total_adj['val']*100/cost_total_adj['val'].sum()).round(2))
cost_intermediate2.drop('val_month',axis=1,inplace=True)
cost_pie_total_adj=cost_intermediate2.loc[cost_intermediate2['ppt']>=2.0]
cost_pie_total_adj=cost_pie_total_adj.append(pd.DataFrame([['Restliche mit <2%',sum(cost_intermediate2.loc[cost_intermediate2['ppt']<2.0]['val']),sum(cost_intermediate2.loc[cost_intermediate2['ppt']<2.0]['ppt'])]],columns=list(cost_intermediate2.columns)),ignore_index=True)

##Create Plots
data_pie=(cost_pie_total_adj,cost_pie_nothol)

plotinfo_pieplot=('Tortendiagramm Kostenkategorien >2% Anteil','Anteilsübersicht mit Urlauben zusammengefasst','Anteilsübersicht ohne Urlaube','yes','Kostenanteile Tortendiagramm.jpg')
plotters.pieplotter(data_pie,data_month,plotinfo_pieplot,printfolder)

print('Alles fertig! Das Programm kann jetzt geschlossen werden.')